package com.roudy.AdeleanESDemo;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

public class ESController {

	static Logger log = Logger.getLogger(ESController.class.getName());

	// Config params for ES connection
	private static final String HOST = "localhost";
	private static final int PORT_ONE = 9200;
	private static final String SCHEME = "http";

	private static RestHighLevelClient restHighLevelClient;

	public synchronized RestHighLevelClient makeConnection() {

		if (restHighLevelClient == null) {
			restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost(HOST, PORT_ONE, SCHEME)));
		}

		return restHighLevelClient;
	}

	public synchronized void closeConnection() {
		try {
			restHighLevelClient.close();
		} catch (IOException e) {
			log.error("Could not close connection");
			log.error(e);
		}
		restHighLevelClient = null;
	}

	public void insertBulkProduct(BulkRequest bulkRequest) {
		try {
			restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
		} catch (IOException e) {
			log.error(e);
		}
	}

}
