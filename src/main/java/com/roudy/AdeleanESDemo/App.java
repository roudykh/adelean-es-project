package com.roudy.AdeleanESDemo;

import org.apache.log4j.Logger;

public class App {

	static Logger log = Logger.getLogger(App.class.getName());

	public static void main(String[] args) {

		log.info("Application up!");

		// Unzip content
		String srcDir = "./src/main/resources/files/xml.zip", destDir = "./src/main/resources/files/";
		String outpath = new Unzipper().unzipFile(srcDir, destDir);
		log.info("Working with: " + outpath);

		// Connect to ElasticSearch
		ESController es = new ESController();
		es.makeConnection();

		// Parse XML file and add index on ES
		new XMLtoES().request(outpath, es);

		es.closeConnection();
	}
}
