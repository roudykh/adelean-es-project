package com.roudy.AdeleanESDemo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipInputStream;

import org.apache.log4j.Logger;

public class Unzipper {

	static Logger log = Logger.getLogger(Unzipper.class.getName());

	final static int BUFFER = 2048;

	public String unzipFile(String compressFilePath, String uncompressFolderPath) {

		String uncompressedFilePath = "";

		try {
			FileInputStream fis = new FileInputStream(compressFilePath);
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
			String filename = zis.getNextEntry().getName();
			uncompressedFilePath = uncompressFolderPath + filename;
			int count;
			byte data[] = new byte[BUFFER];

			// write files to disk
			FileOutputStream fos = new FileOutputStream(uncompressedFilePath);
			BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
			while ((count = zis.read(data, 0, BUFFER)) != -1) {
				dest.write(data, 0, count);
			}

			dest.flush();
			dest.close();
			zis.close();

		} catch (Exception e) {
			log.error(e);
		}

		return uncompressedFilePath;

	}
}
