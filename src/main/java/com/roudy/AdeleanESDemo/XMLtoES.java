package com.roudy.AdeleanESDemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.UUID;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLtoES {

	static Logger log = Logger.getLogger(XMLtoES.class.getName());
	
	private static final String INDEX = "productsdata";

	public void request(String path, final ESController es) {
		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {

				int batch = 1000;
				int counter = 0;
				HashMap<String, String> tempMap = new HashMap<String, String>();
				BulkRequest bulkRequest = new BulkRequest();
				String id, current, currentVal, fullval= "";

				public void startElement(String uri, String localName, String qName, Attributes attributes)
						throws SAXException {

					current = qName;

				}

				public void endElement(String uri, String localName, String qName) throws SAXException {

					if (qName.equalsIgnoreCase("PRODUCT")) {
						counter++;
						
						id = UUID.randomUUID().toString();
						bulkRequest.add(new IndexRequest(INDEX).id(id).source(tempMap));
						
						if (counter % batch == 0) {
							es.insertBulkProduct(bulkRequest);
							log.info("Uploaded: " + counter + " so far");
						}

					} else if (qName.equalsIgnoreCase("PRODUCTS")) {
						es.insertBulkProduct(bulkRequest);
						log.info("Uploaded: " + counter);
					} else {
						tempMap.put(current, fullval);
						fullval = "";
					}

				}

				public void characters(char ch[], int start, int length) throws SAXException {
					
					currentVal = new String(ch, start, length).trim();
					if(!currentVal.equals("")) {
						fullval += currentVal;
					}
				}

			};

			File file = new File(path);
			InputStream inputStream = new FileInputStream(file);
			Reader reader = new InputStreamReader(inputStream, "UTF-8");

			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");

			saxParser.parse(is, handler);

		} catch (Exception e) {
			log.error(e);
		}

	}
}
