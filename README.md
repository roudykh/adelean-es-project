# Adelean ElasticSearch Demo

This is a project that extracts xml file from a zip archive, then parses it and send it to ElasticSearch.

## Environment Setup

Please install Java and your favorite IDE.
We will be using:
* Java 8
* Eclipse
* Maven
* ElasticSearch 7.4.1
* Kibana 7.4.1

Download [ElasticSearch 7.4.1](https://www.elastic.co/fr/downloads/past-releases#elasticsearch) then navigate to bin folder and run using:

```bash
./elasticsearch
```
Elasticsearch is now running! You can access it at [http://localhost:9200](http://localhost:9200) on your web browser.

Download [Kibana 7.4.1](https://www.elastic.co/downloads/past-releases#kibana).

Edit kibana config file and uncomment the following line to point Kibana to your ElasticSearcg instance:
```bash
elasticsearch.hosts: ["http://localhost:9200"]
```

then navigate to bin folder and run using:

```bash
./kibana
```
Kibana is now running! You can access it at [http://localhost:5601](http://localhost:5601) on your web browser.


## Installation
Clone the repository:
```bash
git clone https://roudykh@bitbucket.org/roudykh/adelean-es-project.git
```
Then either open and run the project with an IDE or cd to the project directory: 

```bash
mvn clean install
mvn exec:java -Dexec.mainClass="com.roudy.AdeleanESDemo.App"
```

The documents should be idexed by now!
Go to Kibana console to run some queries.

Matching brand:
![Match query](https://i.imgur.com/eLHsUvY.png)

Delete index:
![Delete index](https://i.imgur.com/p8OhxUA.png)

## Code Explanation
* ### POM.xml Dependencies
For this project we need Java 8, ElasticSearch 7.4.1, and log4j.
```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.roudy</groupId>
	<artifactId>AdeleanES</artifactId>
	<version>1.0</version>
	<name>AdeleanES</name>

	<properties>
		<java.version>1.8</java.version>
		<log4j.version>1.2.17</log4j.version>
		<elasticsearch.version>7.4.1</elasticsearch.version>
	</properties>

	<dependencies>

		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>${log4j.version}</version>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch</groupId>
			<artifactId>elasticsearch</artifactId>
			<version>${elasticsearch.version}</version>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch.client</groupId>
			<artifactId>elasticsearch-rest-high-level-client</artifactId>
			<version>${elasticsearch.version}</version>
		</dependency>

	</dependencies>

</project>
```


* ### Log4j configuration

**Log4j** configuration with INFO logging level and output to log.out file
```
# Define the root logger with appender file
log4j.rootLogger = INFO, FILE

# Define the file appender
log4j.appender.FILE=org.apache.log4j.FileAppender
log4j.appender.FILE.File=./src/main/resources/logs/log.out

# Define the layout for file appender
log4j.appender.FILE.layout=org.apache.log4j.PatternLayout
log4j.appender.FILE.layout.conversionPattern=%d{yyyy-MM-dd HH:mm:ss}-%x-%-5p-%-10c:%m%n
```

* ### Java code
I will start with the main **App** class then dive into the rest of the classes.
```java
public class App {

	static Logger log = Logger.getLogger(App.class.getName());

	public static void main(String[] args) {

		log.info("Application up!");

		// Unzip content
		String srcDir = "./src/main/resources/files/xml.zip", destDir = "./src/main/resources/files/";
		String outpath = new Unzipper().unzipFile(srcDir, destDir);
		log.info("Working with: " + outpath);

		// Connect to ElasticSearch
		ESController es = new ESController();
		es.makeConnection();

		// Parse XML file and add index on ES
		new XMLtoES().request(outpath, es);

		es.closeConnection();
	}
}
```

* Start by unzipping the XML file.
* Connect to ElasticSearch
* Parse XML file and send to ElasticSearch.


**Unzipper** class serves to unzip the XML archive
```
public class Unzipper {

	static Logger log = Logger.getLogger(Unzipper.class.getName());

	final static int BUFFER = 2048;

	public String unzipFile(String compressFilePath, String uncompressFolderPath) {

		String uncompressedFilePath = "";

		try {
			FileInputStream fis = new FileInputStream(compressFilePath);
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
			String filename = zis.getNextEntry().getName();
			uncompressedFilePath = uncompressFolderPath + filename;
			int count;
			byte data[] = new byte[BUFFER];

			// write files to disk
			FileOutputStream fos = new FileOutputStream(uncompressedFilePath);
			BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
			while ((count = zis.read(data, 0, BUFFER)) != -1) {
				dest.write(data, 0, count);
			}

			dest.flush();
			dest.close();
			zis.close();

		} catch (Exception e) {
			log.error(e);
		}

		return uncompressedFilePath;

	}
}
```
During the write operation, the bytes are written to the internal buffer and then writes the whole buffer to the output stream
and then to the disk, this while flushing when necessary.

The **ESController** class controls the ElasticSearch interface
```
public class ESController {

	static Logger log = Logger.getLogger(ESController.class.getName());

	// Config params for ES connection
	private static final String HOST = "localhost";
	private static final int PORT_ONE = 9200;
	private static final String SCHEME = "http";

	private static RestHighLevelClient restHighLevelClient;

	public synchronized RestHighLevelClient makeConnection() {

		if (restHighLevelClient == null) {
			restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost(HOST, PORT_ONE, SCHEME)));
		}

		return restHighLevelClient;
	}

	public synchronized void closeConnection() {
		try {
			restHighLevelClient.close();
		} catch (IOException e) {
			log.error("Could not close connection");
			log.error(e);
		}
		restHighLevelClient = null;
	}

	public void insertBulkProduct(BulkRequest bulkRequest) {
		try {
			restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
		} catch (IOException e) {
			log.error(e);
		}
	}

}
```
Three functions are present in this class:
* makeConnection: return RestHighLevelClient if it exists, or create a new one if not.
                  I used localhost with the default port 9200 on http.
                  I used synchronized to prevent thread interference during write operations for multiple users.
* closeConnection: closing the RestHighLevelClient connection.
* insertBulkProduct: uses restHighLevelClient Bulk API request to ElasticSearch.

The **XMLtoES** class which parses the XML file and creates Index Request to send to ElasticSearch
```
public class XMLtoES {

	static Logger log = Logger.getLogger(XMLtoES.class.getName());

	private static final String INDEX = "productsdata";

	public void request(String path, final ESController es) {
		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {

				int batch = 1000;
				int counter = 0;
				HashMap<String, String> tempMap = new HashMap<String, String>();
				BulkRequest bulkRequest = new BulkRequest();
				String id, current, currentVal, fullval= "";

				public void startElement(String uri, String localName, String qName, Attributes attributes)
						throws SAXException {

					current = qName;

				}

				public void endElement(String uri, String localName, String qName) throws SAXException {

					if (qName.equalsIgnoreCase("PRODUCT")) {
						counter++;

						id = UUID.randomUUID().toString();
						bulkRequest.add(new IndexRequest(INDEX).id(id).source(tempMap));

						if (counter % batch == 0) {
							es.insertBulkProduct(bulkRequest);
							log.info("Uploaded: " + counter + " so far");
						}

					} else if (qName.equalsIgnoreCase("PRODUCTS")) {
						es.insertBulkProduct(bulkRequest);
						log.info("Uploaded: " + counter);
					} else {
						tempMap.put(current, fullval);
						fullval = "";
					}

				}

				public void characters(char ch[], int start, int length) throws SAXException {

					currentVal = new String(ch, start, length).trim();
					if(!currentVal.equals("")) {
						fullval += currentVal;
					}
				}

			};

			File file = new File(path);
			InputStream inputStream = new FileInputStream(file);
			Reader reader = new InputStreamReader(inputStream, "UTF-8");

			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");

			saxParser.parse(is, handler);

		} catch (Exception e) {
			log.error(e);
		}

	}
}
```

The code parses the XML file using SAX parser with UTF-8 encoding.
In the handler code, while iterating the file we add the data to a HashMap tempMap.
Then, each time we reach a closing PRODUCT tag, which means we collected all the data for a product, we add a new IndexRequest for tempMap into the BulkRequest.
We keep doing the above steps until we reach a batch of 1000 products, then we call the insertBulkProduct of the ESController to send the BulkRequest to ElasticSearch.
When the code reaches the closing tag PRODUCTS, it means we reached the end of the file. It then calls the insertBulkProduct of the ESController to send the BulkRequest to ElasticSearch.

A problem occurred while testing:
While parsing html elements, some symbols like &lt;, &gt; li, ul... etc were corrupting the data, so I added a condition in characters function to append all the text to a temporary String until a closing tag is reached.
